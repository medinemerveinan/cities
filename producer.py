from kafka import KafkaProducer
import json
import time

bootstrap_servers = ['localhost:9092']
topicName = 'city'
producer = KafkaProducer(bootstrap_servers=bootstrap_servers)
producer = KafkaProducer()

i = 1
result = {}

with open('cities.log') as f:
    lines = f.readlines()

    for line in lines:
        r = line.split('  ')
        try:
            while True:
                result[i] = {'timestamp': r[0], 'log_level': r[1], 'city': r[2], 'detail': r[3]}
                ack = producer.send(topicName, json.dumps(result[i]))
                metadata = ack.get()
                i += 1
                lines.remove(line)
                time.sleep(0.1)


        except Exception as err:
            pass
