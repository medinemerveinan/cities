from kafka import KafkaConsumer
import sys
import json
import requests

bootstrap_servers = ['localhost:9092']
topicName = 'city'

consumer = KafkaConsumer(topicName, group_id='group1', bootstrap_servers=bootstrap_servers,
                         auto_offset_reset='earliest')

try:
    for message in consumer:
        d = json.loads(message.value)
        url = 'http://127.0.0.1:8000/CityLogCreate/'
        response = requests.post(url, data=d)

except KeyboardInterrupt:
    sys.exit()
