import logging
import logging.handlers as handlers
import time
from logging.handlers import RotatingFileHandler

logger = logging.getLogger('Welcome to')
logger.setLevel(logging.INFO)
# Formatter
formatter = logging.Formatter('%(asctime)s  %(levelname)s  %(message)s  %(name)s %(message)s')

logHandler = RotatingFileHandler('cities.log', maxBytes=2 * 1024 * 1024,
                                 backupCount=5)
logHandler.setLevel(logging.INFO)
logHandler.setFormatter(formatter)

logger.addHandler(logHandler)


def main():
    city_list = ["Istanbul", "Tokyo", "Moskow", "Beijing", "London"]
    while (True):
        for item in city_list:
            time.sleep(0.1)
            msg = "" + "".join(item)
            logger.info(msg)


main()
