from django.db import models

# Create your models here.


class CityLog(models.Model):

    log_timestamp = models.DateTimeField(auto_now_add=True)
    log_level = models.CharField(max_length=255)
    log_city = models.CharField(max_length=255)
    log_detail = models.CharField(max_length=255)