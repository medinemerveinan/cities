import sys
from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response

import logging

import fnmatch
import os.path
from os.path import basename
import time
import subprocess
import select
import logging
import logging.handlers as handlers
import time
from logging.handlers import RotatingFileHandler
from django.http import HttpResponse
from django import forms
from cityapi.forms import CityLogCreateForm
from django.views.generic.edit import CreateView

from braces.views import CsrfExemptMixin

from cityapi.models import CityLog
from django.db.models import Count


class CityLogCreateView(CsrfExemptMixin, CreateView):
    template_name = 'city.html'

    def get(self, request, *args, **kwargs):

        form = CityLogCreateForm()

        # queryset = CityLog.objects.all().annotate(count=Count('log_city'))

        moskow_count = CityLog.objects.filter(log_city='Moskow').count()
        istanbul_count = CityLog.objects.filter(log_city='Istanbul').count()
        london_count = CityLog.objects.filter(log_city='London').count()
        beijing_count = CityLog.objects.filter(log_city='Beijing').count()
        tokyo_count = CityLog.objects.filter(log_city='Tokyo').count()
        print(moskow_count, istanbul_count, london_count, beijing_count, tokyo_count)
        return render(request, self.template_name, {'moskow_count': moskow_count,
                                                    'istanbul_count': istanbul_count,
                                                    'london_count': london_count,
                                                    'beijing_count': beijing_count, 'tokyo_count': tokyo_count})

    def post(self, request, *args, **kwargs):

        data = {
            'log_timestamp': request.POST['timestamp'],
            'log_level': request.POST['log_level'],
            'log_city': request.POST['city'],
            'log_detail': request.POST['detail']
        }
        form = CityLogCreateForm(data)

        if form.is_valid():
            try:
                citylog = form.save()
                citylog.save()
            except Exception as err:
                raise err;

        return HttpResponse(200, 'OK')
