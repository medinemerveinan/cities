from cityapi.models import CityLog
from django import forms


class CityLogCreateForm(forms.ModelForm):
    class Meta:
        model = CityLog
        fields = '__all__'
